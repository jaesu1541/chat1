﻿
// ExamServer3Dlg.h: 헤더 파일
//

#pragma once

#define MAX_USER_COUNT 100 // 최대 클라이언트 수
struct UserData
{
	SOCKET h_socket;
	char ip_address[16];
};

// CExamServer3Dlg 대화 상자
class CExamServer3Dlg : public CDialogEx
{
private:
	int i = 0; // 클라이언트의 정보를 저장하기위해 선언해주었다.
	SOCKET mh_listen_socket; // 클라이언트 소켓이 접속하도록 소켓 핸들 선언
	UserData m_user_list[MAX_USER_COUNT]; // 클라이언트 관리를 위한 배열 선언
	SOCKET mh_socket = INVALID_SOCKET; 
	char m_connect_flag = 0; // 0:접속해제, 1:접속중, 2:접속됨
// 생성입니다.
public:
	CExamServer3Dlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EXAMSERVER3_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	void AddEventString(const char* ap_string);
	DECLARE_MESSAGE_MAP()
private:
	CListBox m_event_list;
protected:
	afx_msg LRESULT On25001(WPARAM wParam, LPARAM lParam);
public:
//	afx_msg void OnDestroy();
	void SendFrameData(SOCKET ah_socket, char a_message_id, unsigned short int a_body_size, char* ap_send_data);
protected:
	afx_msg LRESULT On25002(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedSendBtn();
};

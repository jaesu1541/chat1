﻿
// ExamServer3Dlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "ExamServer3.h"
#include "ExamServer3Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CExamServer3Dlg 대화 상자



CExamServer3Dlg::CExamServer3Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EXAMSERVER3_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CExamServer3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EVENT_LIST, m_event_list);
}

BEGIN_MESSAGE_MAP(CExamServer3Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(25001, &CExamServer3Dlg::On25001)
//	ON_WM_DESTROY()
	ON_MESSAGE(25002, &CExamServer3Dlg::On25002)
	ON_BN_CLICKED(IDC_SEND_BTN, &CExamServer3Dlg::OnBnClickedSendBtn)
END_MESSAGE_MAP()


// CExamServer3Dlg 메시지 처리기

BOOL CExamServer3Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// 소켓의 주소 체계, 타입을 정한다.
	// socket(IPv4 주소체계, TCP방식, 0 : 2번째파라미터를 따라감)
	mh_listen_socket = socket(AF_INET, SOCK_STREAM, 0); 

	// 소켓에 사용될 주소관련 추가 정보를 입력한다.
	sockaddr_in srv_addr; // sockaddr_in 구조체를 사용한다.
	srv_addr.sin_family = AF_INET; // 주소 체계를 명시해준다.
	srv_addr.sin_addr.s_addr = inet_addr("192.168.1.9"); // 자신의 주소값을 명시(서버 IP주소)
	srv_addr.sin_port = htons(18000);  // 프로그램의 포트를 정해준다.

	// 네트워크 시세템에 만들어준 소켓을 바인드한다. 
	bind(mh_listen_socket, (LPSOCKADDR)&srv_addr, sizeof(srv_addr)); 
	TRACE("OninitDialog\n");

	m_connect_flag = 1;

	// 해당 문자열 출력
	AddEventString("서비스를 시작합니다.");

	// 소켓에 수신기능을 부여한다. 이에 따라 서버 소켓이 된다.
	listen(mh_listen_socket, 1); 

	// 소켓이 수신을 받으면 받았다고 알려주는 기능을 한다. 
	WSAAsyncSelect(mh_listen_socket, m_hWnd, 25001, FD_ACCEPT);
	
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CExamServer3Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CExamServer3Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CExamServer3Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CExamServer3Dlg::AddEventString(const char* ap_string)
{
	TRACE("AddEventString\n");
	// 3000개가 넘어가면 제일 위의 문자열을 지워준다.
	while (m_event_list.GetCount() > 3000) // GetCount() : count 변수를 알아낸다.
	{
		m_event_list.DeleteString(0); // 가장 위에 입력된 문자열을 삭제
	}
	int index = m_event_list.InsertString(-1, ap_string); // 가장 마지막에 문자열을 넣는다.
	m_event_list.SetCurSel(index); // 항상 마지막에 입력한 문자열에 초점을 맞추도록 하기위한 함수
}

afx_msg LRESULT CExamServer3Dlg::On25001(WPARAM wParam, LPARAM lParam)
{
	TRACE("On25001\n");
	// 접속한 클라이언트의 IP 주소값을 받기위해 선언한다.
	sockaddr_in client_addr;
	int sockaddr_in_size = sizeof(sockaddr_in);
	// 접속한 클라이언트를 받고, 서버내의 새로운 소켓으로 만든다.
	SOCKET h_socket = accept(mh_listen_socket, (LPSOCKADDR)&client_addr, &sockaddr_in_size);
	TRACE(_T("Accept Client Socket : 0x%X\n"), h_socket);

	
	// 초기화 부분  만약 1명이 접속했을경우 제일 마지막99번 부터 정보가 저장된다.
	//for (i = MAX_USER_COUNT - 1; i > 1; --i) // 구조체 배열의 초기화를 위해 반복문으로 INVALID_SOCKET을 담아둠
	//{
	//	TRACE("Count : %d , m_user_list[i] : %x\n", i, m_user_list[i].h_socket);
		//if (m_user_list[i].h_socket == INVALID_SOCKET)
			//break;
	//}
	
	// 각 클라이언트의 정보를 저장하기 위해 Dlg.h에 i = 0으로 선언을 해주었다.
	TRACE("i : %d, m_user_list[i] :  %x\n", i, m_user_list[i].h_socket);

	// 접속자 수가 100명 이하일 경우 정상적으로 클라이언트 정보를 저장 
	// + WSAAsyncSelect함수가적용되도록함.
	if (i < MAX_USER_COUNT) 
	{
		m_connect_flag = 2; // 연결중

		m_user_list[i].h_socket = h_socket; // 클라이언트 소켓 핸들을 저장한다.
		TRACE("m_user_list[i] : %x, MAX_USER_COUNT : %d\n", m_user_list[i].h_socket, MAX_USER_COUNT);
		// 클라이언트의 IP주소를 저장한다.
		strcpy(m_user_list[i].ip_address, inet_ntoa(client_addr.sin_addr));

		// 현재 이 매세지를 발생시킨 클라이언트 소켓이 읽거나, 
		// 닫는 이벤트를 발생시키면 25002번 메세지를 발생시킨다.
		WSAAsyncSelect(m_user_list[i].h_socket, m_hWnd, 25002, FD_READ | FD_CLOSE); 

		CString str;
		// 대화창에 클라이언트가 접속할 때 클라이언트의 ip주소를 띄운다.
		str.Format("%s에서 접속했습니다.", m_user_list[i].ip_address); 
		AddEventString(str);
		TRACE("m_user_list[i].ip_address : %s\n", m_user_list[i].ip_address);
		
		++i; // 순서대로 배열에 저장하기 위해서 증가해준다.

		TRACE("i  : %d , m_user_list[0] : %x----------------------\n", i, m_user_list[0]);
	}
	else // 초과할 경우 소켓 닫기
	{
		AddEventString("관리 최대 인원 추가");
		closesocket(h_socket);
	}

	return 0;
}



// 클라이언트로부터 받은 문자열을 다른 클라이언트로 뿌려주는 함수
// 보낼 소켓, 보낼 데이터의 기능, 데이터, 데이터의 크기를 인자로 가지는 데이터를 보낼 수 있는 함수
// ah_socket = 어떤 소켓으로 정할것인지, a_message_id : 메세지 ID, a_body_size : 보낼 데이터의 크기, ap_send_data : 보낼 데이터
void CExamServer3Dlg::SendFrameData(SOCKET ah_socket, char a_message_id, unsigned short int a_body_size, char* ap_send_data)
{
	TRACE("SendFrameData : %x\n", ah_socket);
	char* p_send_data = new char[4 + a_body_size]; // 보낼 데이터 크기만큼 동적할당
	*p_send_data = 27; // 키 값을 저장하고 
	*(p_send_data + 1) = a_message_id; // 어떤 기능을 하는지 넣어주고

	// 데이터의 크기도 얼마나 되는지 넣어준다.(1바이트로 할당된 배열을 캐스팅하여 2바이트까지 사용할 수 있도록 함)
	*(unsigned short*)(p_send_data + 2) = a_body_size; 
	// 실제 데이터를 복사한다. 보낼 메세지 p_send_data에 저장
	memcpy(p_send_data + 4, ap_send_data, a_body_size); 

	TRACE("p_send_data : %x\n", p_send_data);
	// 데이터를 보낸다.
	send(ah_socket, p_send_data, a_body_size + 4, 0);


	delete[] p_send_data; // 메모리를 해제
}

afx_msg LRESULT CExamServer3Dlg::On25002(WPARAM wParam, LPARAM lParam)
{
	TRACE("On25002\n");
	// if = FD_READ 클라이언트가 읽는 이벤트를 발생시켰을 때  
	if (WSAGETSELECTEVENT(lParam) == FD_READ)
	{
		if (WSAGETSELECTEVENT(lParam) == FD_READ)
		{
			// FD_READ를 비동기로 받지 않는다.
			// 서로 속도가 느린 컴퓨터들의 정보를 교환한다고했을 때 1000바이트 정보를 200, 300, 200, 300
			// 4번에 걸쳐서 데이터를 수신하는 경우가 생길 수 있다. 이런 경우 FD_READ가 4번 발생하게 되어
			/// 원하던 동작을 제대로 하지 않을 것이다. 따라서 문제의 원인인 FD_READ를 제거한다.
			WSAAsyncSelect(wParam, m_hWnd, 25002, FD_CLOSE); 


			char key;
			recv(wParam, &key, 1, 0); // 수신버퍼의 1byte를 key에 저장한다.
			
			// key변수로 한번 통신할때 27바이트를 받음, 우호적인 클라이언트인지 검사한다.
			if (key == 27) 
			{
				// 수신 버퍼의 1byte를 추가로 읽는다.
				char message_id;
				recv(wParam, &message_id, 1, 0);

				// 수신 버퍼의 2byte를 추가로 읽는다.
				unsigned short int body_size;  // unsigned short int : 2byte
				recv(wParam, (char*)&body_size, 2, 0);

				char* p_body_data = NULL; //데이터의 정보
				if (body_size > 0)
				{
					// 데이터의 크기만큼 메모리를 할당한다.
					p_body_data = new char[body_size]; 

					// 보낸 모든 데이터를 정확하게 읽어오기 위해서 반복문으로 작업을 해준다.
					// 가져온 데이터 값과 실제 데이터의 크기가 같아지면 그만 가져오도록 하기 위한 코드
					int total = 0, x, retry = 0;
					while (total < body_size)
					{
						x = recv(wParam, p_body_data + total, body_size - total, 0);

						// 데이터를 읽으면서 오류가 날 경우 break
						if (x == SOCKET_ERROR) break; 

						total = total + x;
						// 가져온 데이터가 다 안온 경우 5번까지 반복
						if (total < body_size)
						{
							Sleep(50); // 50ms딘위로 처리
							retry++;
							if (retry > 5) break;
						}
					}
				}
				if (message_id == 1)
				{
					int i;
					//TRACE("message_id i : %d\n", i);
					for (i = 0; i < MAX_USER_COUNT; i++)
					{
						TRACE("Count : %d m_user_list[i].h_socket : %x\n", i, m_user_list[i].h_socket);
						if (m_user_list[i].h_socket == wParam) break;
					}
					TRACE("m_user_list.ip : %s , p_body_data : %s\n", m_user_list[i].ip_address, p_body_data);
					CString str;
					str.Format("%s : %s", m_user_list[i].ip_address, p_body_data); // 형식화 하여 str을 만들어준다.
					AddEventString(str); // str 출력


					for (i = 0; i < MAX_USER_COUNT; i++) // 접속한 클라이언트에게 채팅창을 보여주기 위함
					{
						TRACE("Count : %d, m_user_list[i] : %x INVALID_SOCKET : %x\n", i, m_user_list[i].h_socket, INVALID_SOCKET);
						if (m_user_list[i].h_socket == 0xcccccccc/*초기화 되지 않은 지역변수*/) // 저장되지 않은 공간에는 굳이 정보를 보낼 필요가 없으므로 break
							break;
						else if (m_user_list[i].h_socket != INVALID_SOCKET/* -1 : 0xffffffff*/)
						{
							TRACE(_T("mh_listen_socket : %x\n", mh_listen_socket));
							SendFrameData(m_user_list[i].h_socket, 1, str.GetLength() + 1, (char*)(const char*)str);
						}
					}

					

				}

				// 수신된 데이터 처리 
				if (p_body_data != NULL) delete[] p_body_data;

				// FD_READ를 비동기로 다시 받는다. 
				// 추가로 오는 데이터를 다시 받을 수 있게 해주는 것이다.
				WSAAsyncSelect(wParam, m_hWnd, 25002, FD_READ | FD_CLOSE);
			}
		}

	}
	// else : 클라이언트가 닫는 이벤트를 발생시켰을 때 wParam으로 들어온 소켓을 닫아준다.
	else 
	{
		closesocket(wParam);
		CString str;
		for (int i = 0; i < MAX_USER_COUNT; i++)
		{
			if (m_user_list[i].h_socket == wParam)
			{
				TRACE("Count : %d m_user_list : %x, m_user_list[i].ip_address\n", i, m_user_list[i].h_socket);
				m_user_list[i].h_socket = INVALID_SOCKET;
				str.Format("사용자가 종료했습니다. : %s", m_user_list[i].ip_address);
				AddEventString(str);
				break;
			}
		}
	}
	return 0;
}


void CExamServer3Dlg::OnBnClickedSendBtn()
{
	CString str;
	GetDlgItemText(IDC_CHAT_EDIT,  str);
	 TRACE(_T("SendBtn ->  %d\n"), m_connect_flag);
	
	int index = m_event_list.InsertString(-1, _T("Server : " + str)); //m_event_list.InsertString(-1, str);
	 m_event_list.SetCurSel(index); // 초점이 가장 나중을 맞춘다.

	if (m_connect_flag == 2) // 클라이언트 연결이 되어있을 경우에만
	{
		TRACE(_T("SendBtn two\n"));
		
		
		for (i = 0; i < MAX_USER_COUNT; ++i)
		{
			TRACE("i : %d , m_user_list[i].h_socket12 : %x\n", i, m_user_list[i].h_socket);
			/*if (m_user_list[i].h_socket != INVALID_SOCKET && m_user_list[i].h_socket != 0xcccccccc)
			{
				SendFrameData(m_user_list[i].h_socket, 1, str.GetLength() + 1, (char*)(const char*)str);
			}*/
			
			SendFrameData(m_user_list[i].h_socket, 1, str.GetLength() + 1, (char*)(const char*)str);
			if (m_user_list[i + 1].h_socket == INVALID_SOCKET || m_user_list[i + 1].h_socket == 0xcccccccc)
				break;
			
		}
			
		SetDlgItemText(IDC_CHAT_EDIT, ""); // 보내기누르면 입력창 지워짐
		//GotoDlgCtrl(GetDlgItem(IDC_CHAT_EDIT)); // 컨트롤 주소

		
	}
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

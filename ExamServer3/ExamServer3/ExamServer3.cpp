﻿
// ExamServer3.cpp: 애플리케이션에 대한 클래스 동작을 정의합니다.
//

#include "pch.h"
#include "framework.h"
#include "ExamServer3.h"
#include "ExamServer3Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExamServer3App

BEGIN_MESSAGE_MAP(CExamServer3App, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CExamServer3App 생성

CExamServer3App::CExamServer3App()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CExamServer3App 개체입니다.

CExamServer3App theApp;


// CExamServer3App 초기화

BOOL CExamServer3App::InitInstance()
{
	CWinApp::InitInstance();


	WSADATA temp;
	WSAStartup(0x0202, &temp); //소켓 등록 과정, 2.2version

	CExamServer3Dlg dlg;
	m_pMainWnd = &dlg;
	dlg.DoModal();

	WSACleanup(); //소켓 해제

	return FALSE;
}


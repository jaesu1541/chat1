﻿
// ExamClient.cpp: 애플리케이션에 대한 클래스 동작을 정의합니다.
//

#include "pch.h"
#include "framework.h"
#include "ExamClient.h"
#include "ExamClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExamClientApp

BEGIN_MESSAGE_MAP(CExamClientApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CExamClientApp 생성

CExamClientApp::CExamClientApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CExamClientApp 개체입니다.

CExamClientApp theApp;


// CExamClientApp 초기화

BOOL CExamClientApp::InitInstance()
{
	CWinApp::InitInstance();

	WSADATA temp; 
	WSAStartup(0x0202, &temp); // 소켓을 사용하겠다고 선언

	CExamClientDlg dlg;
	m_pMainWnd = &dlg;
	dlg.DoModal();

	WSACleanup(); // 소켓사용 중단 선언

	
	return FALSE;
}


﻿
// ExamClientDlg.h: 헤더 파일
//

#pragma once
#include "afxwin.h"

#define MAX_USER_COUNT 100

struct UserData
{
	SOCKET h_socket;
	char ip_address[16];
};

// CExamClientDlg 대화 상자
class CExamClientDlg : public CDialogEx
{
private:
	struct sockaddr_in srv_addr;
	SOCKET mh_socket = INVALID_SOCKET;
	char m_connect_flag = 0; // 0: 접속해제, 1: 접속중, 2: 접속됨
	UserData m_user_list[MAX_USER_COUNT];
	UINT m_user_count = 0;
// 생성입니다.
public:
	CExamClientDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EXAMCLIENT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	void SendFrameData(SOCKET ah_socket, char a_message_id, unsigned short int a_body_size, char* ap_send_data);
	DECLARE_MESSAGE_MAP()
private:
	CListBox m_chat_list;
public:
	afx_msg void OnBnClickedSendBtn();
	afx_msg void OnDestroy();
	void AddEventString(const char* ap_string);
protected:
	afx_msg LRESULT On25001(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT On25002(WPARAM wParam, LPARAM lParam);
};

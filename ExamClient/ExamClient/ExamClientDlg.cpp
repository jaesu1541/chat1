﻿
// ExamClientDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "ExamClient.h"
#include "ExamClientDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CExamClientDlg 대화 상자



CExamClientDlg::CExamClientDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EXAMCLIENT_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CExamClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHAT_LIST, m_chat_list);
}

BEGIN_MESSAGE_MAP(CExamClientDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SEND_BTN, &CExamClientDlg::OnBnClickedSendBtn)
	ON_WM_DESTROY()
	ON_MESSAGE(25001, &CExamClientDlg::On25001)
	ON_MESSAGE(25002, &CExamClientDlg::On25002)
END_MESSAGE_MAP()


// CExamClientDlg 메시지 처리기

BOOL CExamClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// 소켓을 쓰기 위한 초기설정
	mh_socket = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in srv_addr;
	memset(&srv_addr, 0, sizeof(struct sockaddr_in));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = inet_addr("192.168.1.9");
	srv_addr.sin_port = htons(18000);

	WSAAsyncSelect(mh_socket, m_hWnd, 25001, FD_CONNECT); // 서버 미개통시 최대 28초간 발생하는 응답없음을 피하기 위함
	m_connect_flag = 1; //서버 접속 시도중
	AddEventString("서버 접속중입니다. . . ");
	connect(mh_socket, (LPSOCKADDR)&srv_addr, sizeof(srv_addr));

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CExamClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 애플리케이션의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CExamClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CExamClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// 전송함수
// ah_socket = 어떤 소켓으로 정할것인지, a_message_id : 메세지 ID, a_body_size : 보낼 데이터의 크기, ap_send_data : 보낼 데이터
void CExamClientDlg::SendFrameData(SOCKET ah_socket, char a_message_id, unsigned short int a_body_size, char* ap_send_data)
{
	TRACE("SendFrameData : %x\n", ah_socket);
	char* p_send_data = new char[4 + a_body_size]; // 보낼 데이터 크기만큼 동적할당
	*p_send_data = 27;
	*(p_send_data + 1) = a_message_id;
	*(unsigned short*)(p_send_data + 2) = a_body_size; // 1바이트로 할당된 배열 을 캐스팅하여 2바이트까지 사용할 수 있도록함
	memcpy(p_send_data + 4, ap_send_data, a_body_size); // 보낼 메세지 p_send_data에 저장

	TRACE("p_send_data : %x", p_send_data);
	send(ah_socket, p_send_data, a_body_size + 4, 0); //전송함수 

	delete[] p_send_data;
}

void CExamClientDlg::OnBnClickedSendBtn()
{
	CString str;
	GetDlgItemText(IDC_CHAT_EDIT, str);

	if (m_connect_flag == 2) // 서버에 연결이 되어있을 경우만
	{
		SendFrameData(mh_socket, 1, str.GetLength() + 1, (char*)(const char*)str);
		SetDlgItemText(IDC_CHAT_EDIT, ""); // 보내기 누르면 입력창 지워짐 
		// GotoDlgCtrl(GetDlgItem(IDC_CHAT_EDIT)); // 컨트롤의 주소
	}
}

// 프로그램을 종료할 경우 열려있는 소켓을 닫는 함수
void CExamClientDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (mh_socket != INVALID_SOCKET) //소켓이 만들어져 있다면
	{
		TRACE("INVALID_SOCKET : %x\n", INVALID_SOCKET);
		closesocket(mh_socket); //소켓을 닫는다
		mh_socket = INVALID_SOCKET; // 만약을 위해 보정
	}
}

// List Box에 띄울 문자열을 제어하는 함수
void CExamClientDlg::AddEventString(const char* ap_string)
{
	while (m_chat_list.GetCount() > 500) // List Box에 문자열이 500 초과시 
	{
		m_chat_list.DeleteString(0); // List Box의 최초 문자열 제거
	}

	int index = m_chat_list.InsertString(-1, ap_string); // 인덱스는 가장 나중에 입력된 문자열
	m_chat_list.SetCurSel(index); // 항상 마지막에 입력한 문자열에 초점을 맞추도록 함
}

// 서버 접속시 발생하는 과정
afx_msg LRESULT CExamClientDlg::On25001(WPARAM wParam, LPARAM lParam)
{

	// 서버가 열려있지 않을 경우 다시 접속해제 상태로 만들고 혹시나 열려있을지도 모르는 소켓을 닫는다.
	if (WSAGETSELECTERROR(lParam)) // True : 에러발생
	{
		m_connect_flag = 0;
		closesocket(mh_socket);
		mh_socket = INVALID_SOCKET;
		AddEventString("서버 접속을 실패했습니다.");
	}
	else // 에러 없음
	{
		m_connect_flag = 2; //접속된 상태
		WSAAsyncSelect(mh_socket, m_hWnd, 25002, FD_READ | FD_CLOSE); // 소켓을 닫을 경우(FD_CLOSE), 문자열이 날라올 경우 
		AddEventString("서버에 접속했습니다.");                                                  // 를 받기 위해 대기
	}

	return 0;
}


afx_msg LRESULT CExamClientDlg::On25002(WPARAM wParam, LPARAM lParam)
{
	if (WSAGETSELECTEVENT(lParam) == FD_READ)
	{
		if (WSAGETSELECTEVENT(lParam) == FD_READ)
		{
			WSAAsyncSelect(wParam, m_hWnd, 25002, FD_CLOSE);

			char key;
			recv(wParam, &key, 1, 0);
			if (key == 27)
			{
				char message_id;
				recv(wParam, &message_id, 1, 0);

				unsigned short int body_size;
				recv(wParam, (char*)&body_size, 2, 0);

				char* p_body_data = NULL;
				if (body_size > 0)
				{
					p_body_data = new char[body_size];

					int total = 0, x, retry = 0;
					while (total < body_size)
					{
						x = recv(wParam, p_body_data + total, body_size - total, 0);
						if (x == SOCKET_ERROR) break;
						total = total + x;
						if (total < body_size)
						{
							Sleep(50);
							retry++;
							if (retry > 5) break;
						}
					}
				}
				TRACE("message_id : %d\n", message_id);
				// 서버와의 데이터 통신 규약을 맞춘 데이터 방식으로 서버에서 받은 문자열을 List Box에 띄워주도록 하는 부분
				if (message_id == 1)
				{


					TRACE("mh_socket : %x\n", mh_socket);
					/*if (mh_socket)
					{
						CString str;
						TRACE("p_body_data : %s", p_body_data);
						str.Format("%s", p_body_data);
						AddEventString(str);
					}*/
					//else
						AddEventString(p_body_data);

				}

				// 수신된 데이터 처리 
				if (p_body_data != NULL) delete[] p_body_data;

				WSAAsyncSelect(wParam, m_hWnd, 25002, FD_READ | FD_CLOSE);
			}
		}

	}
	else
	{
		closesocket(wParam);
		CString str;
		for (int i = 0; i < MAX_USER_COUNT; i++)
		{

			if (m_user_list[i].h_socket == wParam)
			{
				m_user_list[i].h_socket == INVALID_SOCKET;
				str.Format("사용자가 종료했습니다. : %s", m_user_list[i].ip_address);
				AddEventString(str);
				break;
			}
		}
	}
	return 0;


}

